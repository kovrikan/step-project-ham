//button`s view in "Our Services"
$(document).ready(function () {
    let serviceButton = $(".kind p");
    serviceButton.click(function () {
        $(".kind").removeClass("as-menu");
        serviceButton.removeClass("active-service");
        serviceButton.addClass("pb");
        $(this).addClass("active-service");
        $(this).removeClass("pb");
    });
//choice of service in "Our Services"
         $(".pp1").click(function () {
         $(".k1").addClass("as-menu");
         $(".desc-of-service").css("display", "none");
         $(".desc-of-service1").css("display", "flex")
    });
         $(".pp2").click(function () {
         $(".k2").addClass("as-menu");
         $(".desc-of-service").css("display", "none");
         $(".desc-of-service2").css("display", "flex")
    });
         $(".pp3").click(function () {
         $(".k3").addClass("as-menu");
         $(".desc-of-service").css("display", "none");
         $(".desc-of-service3").css("display", "flex")
    });
         $(".pp4").click(function () {
         $(".k4").addClass("as-menu");
         $(".desc-of-service").css("display", "none");
         $(".desc-of-service4").css("display", "flex")
    });
         $(".pp5").click(function () {
         $(".k5").addClass("as-menu");
         $(".desc-of-service").css("display", "none");
         $(".desc-of-service5").css("display", "flex")
         });
         $(".pp6").click(function () {
         $(".k6").addClass("as-menu");
         $(".desc-of-service").css("display", "none");
         $(".desc-of-service6").css("display", "flex")
         });
});

// adding photos to "Our amazing work"
function getOff1 () {
    $(".off1").css("display", "block") &&
    $(".load-more").addClass("add3");
    $(".add3").removeClass("load-more");
    $(".gooey").css("display", "none")&&
    $(".loader").css("display", "block");
}
function getOff () {
    $(".off").css("display", "block") &&
    $(".add3").css("display", "none") &&
    $(".blocks-of-work").css("padding-bottom", "85px");
}
function loading () {
    $(".loader").css("display", "none")&&
    $(".gooey").css("display", "block");
}
    $(document).ready(function () {
        $(".load-more").click(function () {
            loading();
            setTimeout(getOff1, 1500);
            $(".add3").click(function () {
                loading();
                setTimeout(getOff, 1500)
            });
        });
    });
//filtering items in "Our amazing work"
    $(".grid").isotope({
        itemSelector: ".grid-item",
        layoutMode: "fitRows",
    });
// button`s view changing in "Our amazing work"
    $(document).ready(function () {
        let button = $(".work-button");
        button.click(function () {
            button.removeClass("active");
            $(this).addClass("active");
//
            $(".block").removeClass("off1 && off");
            let selector = $(this).attr("data-filter");
            $(".grid").isotope({
                filter: selector
            });
        });
        $(".all").click(function () {
            $(".off") && $(".off1").css("display", "none")
        });
        $(".gd").click(function () {
            $(".load-more").css("display", "none")  &&
            $(".blocks-of-work").css("padding-bottom", "85px");
        });
        $(".wd").click(function () {
            $(".load-more").css("display", "none")  &&
            $(".blocks-of-work").css("padding-bottom", "85px");
        });
        $(".lp").click(function () {
            $(".load-more").css("display", "none")  &&
            $(".blocks-of-work").css("padding-bottom", "85px");
        });
        $(".wp").click(function () {
            $(".load-more").css("display", "none")  &&
            $(".blocks-of-work").css("padding-bottom", "85px");
        })
    });

// slider in "What people said..."

// slide by clicking in icons
$(document).ready(function () {
    $(".person1-slider").click(get1person);
    $(".person2-slider").click(get2person);
    $(".person3-slider").click(get3person);
    $(".person4-slider").click(get4person);

// slide by clicking in arrows
    $(".slide-from1-to2").click(get2person);
    $(".slide-from1-to4").click(get4person);
    $(".slide-from2-to3").click(get3person);
    $(".slide-from2-to1").click(get1person);
    $(".slide-from3-to4").click(get4person);
    $(".slide-from3-to2").click(get2person);
    $(".slide-from4-to1").click(get1person);
    $(".slide-from4-to3").click(get3person);
});
