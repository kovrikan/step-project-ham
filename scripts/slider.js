
function get1person () {
    $(".person-block").css("display", "none") &&
    $(".person1").css("display", "block")&&
    $(".person-slider").removeClass("person4-slider-active")&&
    $(".person-slider").removeClass("person2-slider-active")&&
    $(".person-slider").removeClass("person3-slider-active")&&
    $(".person1-slider").addClass("person1-slider-active")&&
    $(".slide-left").removeClass("slide-from1-to4")&&
    $(".slide-left").removeClass("slide-from2-to1")&&
    $(".slide-left").removeClass("slide-from3-to2")&&
    $(".slide-left").removeClass("slide-from4-to3")&&
    $(".slide-right").removeClass("slide-from1-to2")&&
    $(".slide-right").removeClass("slide-from2-to3")&&
    $(".slide-right").removeClass("slide-from3-to4")&&
    $(".slide-right").removeClass("slide-from4-to1")&&
    $(".slide-right").addClass("slide-from1-to2")&&
    $(".slide-left").addClass("slide-from1-to4");
}

function get2person () {
    $(".person-block").css("display", "none") &&
    $(".person2").css("display", "block")&&
    $(".person-slider").removeClass("person4-slider-active")&&
    $(".person-slider").removeClass("person1-slider-active")&&
    $(".person-slider").removeClass("person3-slider-active")&&
    $(".person2-slider").addClass("person2-slider-active")&&
    $(".slide-left").removeClass("slide-from1-to4")&&
    $(".slide-left").removeClass("slide-from2-to1")&&
    $(".slide-left").removeClass("slide-from3-to2")&&
    $(".slide-left").removeClass("slide-from4-to3")&&
    $(".slide-right").removeClass("slide-from1-to2")&&
    $(".slide-right").removeClass("slide-from2-to3")&&
    $(".slide-right").removeClass("slide-from3-to4")&&
    $(".slide-right").removeClass("slide-from4-to1")&&
    $(".slide-right").addClass("slide-from2-to3")&&
    $(".slide-left").addClass("slide-from2-to1");
}

function get3person () {
    $(".person-block").css("display", "none") &&
    $(".person3").css("display", "block")&&
    $(".person-slider").removeClass("person4-slider-active")&&
    $(".person-slider").removeClass("person1-slider-active")&&
    $(".person-slider").removeClass("person2-slider-active")&&
    $(".person3-slider").addClass("person3-slider-active")&&
    $(".slide-left").removeClass("slide-from1-to4")&&
    $(".slide-left").removeClass("slide-from2-to1")&&
    $(".slide-left").removeClass("slide-from3-to2")&&
    $(".slide-left").removeClass("slide-from4-to3")&&
    $(".slide-right").removeClass("slide-from1-to2")&&
    $(".slide-right").removeClass("slide-from2-to3")&&
    $(".slide-right").removeClass("slide-from3-to4")&&
    $(".slide-right").removeClass("slide-from4-to1")&&
    $(".slide-right").addClass("slide-from3-to4")&&
    $(".slide-left").addClass("slide-from3-to2");
}

function get4person () {
    $(".person-block").css("display", "none") &&
    $(".person4").css("display", "block")&&
    $(".person-slider").removeClass("person1-slider-active")&&
    $(".person-slider").removeClass("person2-slider-active")&&
    $(".person-slider").removeClass("person3-slider-active")&&
    $(".person4-slider").addClass("person4-slider-active")&&
    $(".slide-left").removeClass("slide-from1-to4")&&
    $(".slide-left").removeClass("slide-from2-to1")&&
    $(".slide-left").removeClass("slide-from3-to2")&&
    $(".slide-left").removeClass("slide-from4-to3")&&
    $(".slide-right").removeClass("slide-from1-to2")&&
    $(".slide-right").removeClass("slide-from2-to3")&&
    $(".slide-right").removeClass("slide-from3-to4")&&
    $(".slide-right").removeClass("slide-from4-to1")&&
    $(".slide-right").addClass("slide-from4-to1")&&
    $(".slide-left").addClass("slide-from4-to3");
}